#
# DOS Makefile (Turbo C 2.01)
#

all: amb.com

amb.com: amb.c ptui/ptui-dos.c
	tcc -DREALDOS -DVLINE=0xB3 -DARRUP=0x18 -DARRDW=0x19 -mt -lt -O -Z -d -w -f- -Iptui amb.c ptui\ptui-dos.c

clean:
	del *.obj
	del amb.com
