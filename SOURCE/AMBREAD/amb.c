/*
 * amb is a reader of AMB (Ancient Machine Book) files. MIT license.
 *
 * Copyright (C) 2020-2025 Mateusz Viste
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/* program version */
#define PVER "20250110"

#ifdef REALDOS
/* real-mode DOS specific (OpenWatcom and Turbo C) */
#include <dos.h>
#else
#include <stdio.h> /* DOS code does not rely on stdio */
#endif

#include <ctype.h>   /* tolower() */
#include <stdlib.h>
#include <string.h>

#include "ptui/ptui.h"

#ifndef VLINE /* vertical line used as left border of the screen */
  #ifdef UNICODEOUTPUT
    #define VLINE 0x2502    /* unicode BOX DRAWINGS LIGHT VERTICAL LINE (note that the "HEAVY" variant is not available in the default Windows XP console) */
  #else
    #define VLINE '|'
  #endif
#endif

#ifndef ARRUP /* 'up' arrow */
  #ifdef UNICODEOUTPUT
    #define ARRUP 0x2191    /* unicode UPWARDS ARROW */
  #else
    #define ARRUP '^'
  #endif
#endif

#ifndef ARRDW /* 'down' arrow */
  #ifdef UNICODEOUTPUT
    #define ARRDW 0x2193    /* unicode DOWNWARDS ARROW */
  #else
    #define ARRDW 'V'
  #endif
#endif

/* how many history locations to keep in memory */
#ifndef HISTORYSIZE
  #define HISTORYSIZE 16
#endif

/* uncomment to print on screen keycodes of unhandled key presses (debug only) */
/* #define PRINT_KEYCODES */


enum AMB_COL {
  AMB_TITLEBAR = 0,
  AMB_LBORDER = 1,
  AMB_RBORDER = 2,
  AMB_RBORDERC = 3,
  AMB_WARN = 4
};

enum AMA_MODE {
  AMA_NORMAL = 6, /* values 0..5 are reserved for program's colors (see above) */
  AMA_BORING = 7,
  AMA_LINK = 8,
  AMA_HEAD = 9,
  AMA_LINK_CURSOROVER = 10,
  AMA_NOTICE = 11,
  AMA_HIDDEN = 12 /* this MUST be last! (used to size the colscheme array) */
};


/* actions returned by display_ama() */
enum ACTION {
  ACTION_NONE,
  ACTION_LOADPAGE,
  ACTION_PREV,
  ACTION_QUIT
};


#ifdef REALDOS

typedef unsigned short FILEHANDLE;
#define FILEHANDLE_NONE 0xffffu

static FILEHANDLE FOPEN(char *fname) {
  union REGS r;
  r.h.ah = 0x3D; /* DOS 2+ OPEN EXISTING FILE */
  r.h.al = 0;
  r.x.dx = FP_OFF(fname);
  int86(0x21, &r, &r);
  if (r.x.cflag != 0) return(FILEHANDLE_NONE);
  return(r.x.ax);
}

static unsigned short FREAD(void far *buf, unsigned short readlen, FILEHANDLE handle) {
  union REGS r;
  struct SREGS sr;
  r.h.ah = 0x3f; /* DOS 2+ read from file or device */
  r.x.bx = handle;
  r.x.cx = readlen;
  sr.ds = FP_SEG(buf);
  r.x.dx = FP_OFF(buf);
  int86x(0x21, &r, &r, &sr);
  if (r.x.cflag != 0) return(0);
  return(r.x.ax);
}

static void FCLOSE(FILEHANDLE h) {
  union REGS r;
  r.h.ah = 0x3E; /* DOS 2+ CLOSE FILE BY HANDLE */
  r.x.bx = h;
  int86(0x21, &r, &r);
}

static void FSEEK(FILEHANDLE h, unsigned long offset) {
  union REGS r;
  r.h.ah = 0x42; /* DOS 2+ SET CURRENT FILE POSITION */
  r.h.al = 0; /* origin offset is start of file */
  r.x.bx = h;
  r.x.cx = (unsigned short)(offset >> 16);  /* offset high word */
  r.x.dx = (unsigned short)(offset & 0xffffu); /* offset low word */
  int86(0x21, &r, &r);
}

/* use the DOS call to avoid pulling in puts() and friends */
static void PUTS(char *s) {
  const char crlf[] = "\r\n$";
  union REGS r;
  int i;
  for (i = 0; s[i] != 0; i++); /* replace the NULL terminator by a $ */
  s[i] = '$';
  r.h.ah = 0x09;
  r.x.dx = FP_OFF(s);
  int86(0x21, &r, &r);
  s[i] = 0; /* replace back the NULL terminator */
  /* output a crl/lf now */
  r.h.ah = 0x09;
  r.x.dx = FP_OFF(crlf);
  int86(0x21, &r, &r);
}

#else

typedef FILE* FILEHANDLE;
#define FILEHANDLE_NONE NULL
#define FOPEN(x) fopen(x, "rb")
#define FREAD(b, l, h) fread(b, 1, l, h)
#define FCLOSE(x) fclose(x);
#define FSEEK(h, o) fseek(h, o, SEEK_SET)
#define PUTS(x) puts(x)

#define far /* replace far with nothing */

#endif

/* unicode platforms use a map table to match codepage bytes into unicode glyphs */
#ifdef UNICODEOUTPUT
static unsigned short UNICODEMAP[256];
#endif


/* this is like strcpy(), but allows destination to be a far pointer */
static void mystrcpy(char far *dst, char *src) {
  int i;
  for (i = 0;; i++) {
    dst[i] = src[i];
    if (src[i] == 0) break;
  }
}


#ifdef REALDOS
/* allocates a 64K buffer and returns a ptr to it or NULL on failure */
static void far *ALLOC64K(void) {
  /* NOTE OpenWatcom's malloc() is unable to allocate a full 64K block due
   * to OpenWatcom's book-keeping overhead, that's why a direct DOS call is
   * performed instead */
  union REGS r;
  r.h.ah = 0x48;
  r.x.bx = 4096; /* request 4096 paragraphs (16 bytes each, ie. 64K total) */
  int86(0x21, &r, &r); /* CF should be clear, AX = segment of allocated block */
  if (r.x.cflag != 0) return(NULL);
  return(MK_FP(r.x.ax, 0));
}
#endif


/* returns the length of a link target (ie. content until a ':' character) */
static int linktargetlen(const char far *s) {
  int r;
  for (r = 0;; r++) {
    switch (s[r]) {
      case 0:
      case '\n':
        return(r);
      case ':':
        return(r+1);
    }
  }
}


/* returns number of bytes before next escape code (%) or end of line (\n or 0) */
static int bytesuntilesc(const char far *s) {
  int r;
  for (r = 0;; r++) {
    switch (s[r]) {
      case 0:
      case '\n':
      case '%':
        return(r);
    }
  }
}


/* display a warning. line2 is optional. */
static void warn(const char *line1, const char *line2, const unsigned char *colscheme) {
  int y = (ptui_getrowcount() / 2) - 2;
  int len;
  int x;
  int t;

  len = strlen(line1);
  if (line2 != NULL) {
    int l2 = strlen(line2);
    if (l2 > len) len = l2;
  }
  /* draw a nice box */
  x = 38 - (len / 2);
  ptui_putchar_rep(' ', colscheme[AMB_WARN], x, y, len + 4);
  y++;
  ptui_putchar_rep(' ', colscheme[AMB_WARN], x, y, len + 4);
  /* print line 1 */
  for (t = 0; line1[t] != 0; t++) ptui_putchar(line1[t], colscheme[AMB_WARN], x + 2 + t, y);
  y++;
  /* print line 2, if defined */
  if (line2 != NULL) {
    ptui_putchar_rep(' ', colscheme[AMB_WARN], x, y, len + 4);
    for (t = 0; line2[t] != 0; t++) ptui_putchar(line2[t], colscheme[AMB_WARN], x + 2 + t, y);
    y++;
  }
  /* end of box */
  ptui_putchar_rep(' ', colscheme[AMB_WARN], x, y, len + 4);
}


/* display a line of AMA text, until first LF, CR or NULL terminator
 * returns amount of consumed bytes */
static unsigned short display_ama_line(const char far *l, int linenum, const unsigned char *colscheme, int cursorx, int cursory, const char far **linktarget) {
  enum AMA_MODE curmode = AMA_NORMAL;
  int waitformode = 0;
  int displayedlen = 0;
  int consumedbytes = 0;

  for (;; consumedbytes++) {
    /* ignore \r */
    if (l[consumedbytes] == '\r') continue;
    /* too much data already */
    /* end of line? */
    if ((l[consumedbytes] == '\n') || (l[consumedbytes] == 0)) {
      /* print the rest of the on-screen line, if any left, using the normal mode color */
      if (displayedlen < 78) {
        ptui_putchar_rep(' ', colscheme[AMA_NORMAL], 1 + displayedlen, linenum, 78 - displayedlen);
        displayedlen = 78;
      }
      if (l[consumedbytes] == '\n') consumedbytes++; /* skip \n so next call gets next line */
      return(consumedbytes);
    }
    /* am I waiting for a mode (after %)? */
    if (waitformode) {
      waitformode = 0;
      switch (l[consumedbytes]) { /* what kind of control code did I get? */
        case 'h': /* heading */
          curmode = AMA_HEAD;
          break;
        case 'b': /* boring text */
          curmode = AMA_BORING;
          break;
        case 't': /* normal text */
          curmode = AMA_NORMAL;
          break;
        case '!': /* warning/notice */
          curmode = AMA_NOTICE;
          break;
        case 'l': /* link */
          curmode = AMA_LINK;
          {
            const char far *target = l + consumedbytes + 1;
            consumedbytes += linktargetlen(l + consumedbytes + 1);
            if ((cursory == linenum) && (cursorx > displayedlen)) {
              if (cursorx < displayedlen + bytesuntilesc(l + consumedbytes)) {
                curmode = AMA_LINK_CURSOROVER;
                *linktarget = target;
              }
            }
          }
          break;
        case '%': /* percent */
          if (displayedlen < 78) ptui_putchar('%', colscheme[curmode], 1 + displayedlen++, linenum);
          break;
        default: /* unknown */
          break;
      }

      continue;
    }
    /* is this a control code? */
    if (l[consumedbytes] == '%') {
      waitformode = 1;
      continue;
    }
    /* actual content */
    if (displayedlen < 78) {
#ifdef UNICODEOUTPUT
      ptui_putchar(UNICODEMAP[(unsigned char)(l[consumedbytes])], colscheme[curmode], 1 + displayedlen++, linenum);
#else
      ptui_putchar(l[consumedbytes], colscheme[curmode], 1 + displayedlen++, linenum);
#endif
    }
  }
}


/* checks two filenames for match. filenames are up to 12 characters.
 * case-insensitive. returns 0 on match, non-zero otherwise. */
static int f12match(const char *f1, const char *f2) {
  int i;
  for (i = 0; i < 12; i++) {
    if (tolower(f1[i]) != tolower(f2[i])) return(1);
    if (f1[i] == 0) return(0);
  }
  return(0);
}


/* looks into the AMB container and returns the slot number at which the
 * file is present. sets errflag on error. */
static unsigned short ama_findslot(const char *fname, unsigned short fcount, FILEHANDLE fd, int *errflag) {
  char fentry[20]; /* only looking at the fname so signed char is ok */
  unsigned short i;

  for (i = 0; i < fcount; i++) {
    /* read file entries until fname is found */
    FSEEK(fd, 4 + 2 + (i * 20));
    if (FREAD(fentry, 20, fd) != 20) goto ERR_QUIT;
    /* is this the file I am looking for? */
    if (f12match(fentry, fname) == 0) break;
  }
  if (i == fcount) goto ERR_QUIT; /* NOT FOUND */
  *errflag = 0;
  return(i);

  ERR_QUIT:
  *errflag = 1;
  return(0);
}


static unsigned short consumecontrol(const char far *body) {
  unsigned short i = 0;
  if (body[i] != '%') return(0);
  i++;
  switch (body[i]) {
    case 'l':
      i += linktargetlen(body + i);
      break;
    case '\n':
    case '\0':
      break;
    default:
      i++;
      break;
  }
  return(i);
}


/* returns line number where next link is, or 0xffff if no link could be found */
static unsigned short find_next_link(const char far *body_org, const char far *displine_first_ptr, unsigned short displine_first, int *cursorx, int cursory) {
  const char far *body;
  unsigned short i, t, curline;
  int toplookout = 0; /* information whether or not I looked from top yet */

  /* jump to line where the cursor is now */
  body = displine_first_ptr;
  curline = displine_first;
  i = 0; /* counts all data characters */
  for (; curline != (displine_first + cursory - 1); i++) {
    if (body[i] == 0) break;
    if (body[i] == '\n') curline++;
  }

  /* skip cursorx displayable bytes (or entire line) */
  t = 0; /* counts displayable characters */
  for (;;) {
    unsigned short bytes = bytesuntilesc(body + i);
    t += bytes;
    i += bytes;
    if (t >= *cursorx) break;
    if ((body[i] == '\n') || (body[i] == 0)) {
      t = 0;
      break;
    }
    if (body[i] == '%') { /* skip control codes */
      if (body[i + 1] == '%') t++;
      i += consumecontrol(body + i);
      continue;
    }
  }
  /* look for nearest %l */
  AGAIN:
  for (;;) {
    unsigned short bytes = bytesuntilesc(body + i);
    t += bytes;
    i += bytes;
    if ((body[i] == '%') && (body[i + 1] == 'l')) {
      *cursorx = t + 1;
      return(curline);
    }
    if (body[i] == '\n') {
      t = 0;
      i++;
      curline++;
      continue;
    }
    if (body[i] == 0) break;
    if (body[i] == '%') { /* skip control codes */
      if (body[i + 1] == '%') t++;
      i += consumecontrol(body + i);
    }
  }
  /* if I'm here, then no link could be found - look again, but this time through the entire document */
  if (toplookout != 0) return(0xffffu);
  toplookout = 1;
  body = body_org;
  curline = 0;
  t = 0;
  i = 0;
  goto AGAIN;
}


/* returns the length of line, in bytes */
static unsigned short linelen(const char far *ptr) {
  unsigned short r;
  for (r = 0;; r++) {
    if (ptr[r] == '\n') return(r + 1);
    if (ptr[r] == 0) return(r);
  }
}


static enum ACTION display_ama(const char far *body, const unsigned char *colscheme, unsigned short *newtarget, FILEHANDLE fd, unsigned short ambfcount, int esc_quits) {
  int curline;
  unsigned short displine_first = 0;
  unsigned short displine_last;
  unsigned short lastcontentline;
  int emptylines_bottom; /* number of empty lines displayed at the bottom of the document */
  int key;
  int totlinescount = 0;
  const char far *ptr;
  int cursorx = 1, cursory = 1;
  const char far *linkover; /* pointer to link currently cursor-over-ed */
  unsigned short refreshonlylines[2] = {0xffffu, 0xffffu};
  int fileposcursor; /* y-position of the right-edge file cursor */

  /* one-time thing: count the number of lines in the file */
  for (ptr = body; *ptr != 0; ptr++) if (*ptr == '\n') totlinescount++;

  ptr = NULL;

  AGAIN:
  displine_last = displine_first + ptui_getrowcount() - 2;
  linkover = NULL;
  curline = displine_first;
  emptylines_bottom = 0;
  ptui_locate(cursorx, cursory);

  /* compute file position and draw right border */
  {
    int i;
    int range = ptui_getrowcount() - 3; /* 3 = titlebar + top and bottom arrows */
    if ((displine_first + cursory - 1 == 0) || (totlinescount == 0)) {
      fileposcursor = 2;
    } else if (displine_first + cursory - 1 >= totlinescount) {
      fileposcursor = range + 1;
    } else {
      fileposcursor = (((displine_first + cursory - 1) * range) / totlinescount) + 2;
    }
    for (i = 1; i < ptui_getrowcount(); i++) {
      if (i == fileposcursor) {
        ptui_putchar(' ', colscheme[AMB_RBORDERC], 79, i);
      } else {
        ptui_putchar(' ', colscheme[AMB_RBORDER], 79, i);
      }
    }
    /* draw arrows */
    ptui_putchar(ARRUP, colscheme[AMB_RBORDER], 79, 1);
    ptui_putchar(ARRDW, colscheme[AMB_RBORDER], 79, range + 2);
  }

  if (ptr == NULL) { /* no position in cache, find it */
    /* skip lines until curline = displine_first */
    ptr = body;
    curline = 0;
    while (curline < displine_first) {
      if (*ptr == '\n') curline++;
      if (*ptr == 0) break;
      ptr++;
    }
  }

  /* display lines until displine_last or end of document */
  {
  const char far *vptr = ptr;
  for (;;) {
    if (*vptr == 0) break; /* end of document */
    if ((refreshonlylines[0] != 0xffffu) && (refreshonlylines[0] != curline) && (refreshonlylines[1] != curline)) {
      vptr += linelen(vptr);
    } else {
      vptr += display_ama_line(vptr, 1 + curline - displine_first, colscheme, cursorx, cursory, &linkover);
    }
    if (curline > displine_last) break; /* screen bottom reached */
    curline++;
  }
  /* remember last line where cursor presence is allowed */
  lastcontentline = curline - displine_first + 1; /* allow to go 1 line past actual content */
  }

  /* relocate cursor if it is out of content screen now (could happen after a pgdown event) */
  if (cursory > lastcontentline) {
    cursory = lastcontentline;
    ptui_locate(cursorx, cursory);
  }

  /* draw rest of display lines as empty space */
  for (; curline <= displine_last; curline++) {
    ptui_putchar_rep(' ', colscheme[AMA_NORMAL], 1, 1 + curline - displine_first, 78);
    emptylines_bottom++;
  }
  ptui_refresh();

  /* pre-set refreshonly variables to "refresh all screen" */
  refreshonlylines[0] = 0xffffu;
  refreshonlylines[1] = 0xffffu;

  ptui_mouseshow(1);
  WAITEVENT:
  key = ptui_getkey();
  if (key == PTUI_MOUSE) { /* mouse event */
    unsigned int x, y;
    if ((ptui_getmouse(&x, &y) == 0) && (x > 0) && (y > 0) && (x < 79) && (y <= lastcontentline)) {
      if ((cursorx == (int)x) && (cursory == (int)y) && (linkover != NULL)) {
        key = 0x0d;
      } else {
        refreshonlylines[0] = displine_first + cursory - 1;
        refreshonlylines[1] = displine_first + y - 1;
      }
      cursorx = x;
      cursory = y;
    } else if ((x == 79) && (y > 0)) { /* cursor bar click */
      if ((y == 1) && (displine_first > 0)) {
        displine_first--;
        ptr = NULL; /* displine_first ptr needs to be recomputed now */
      } else if (((int)y == ptui_getrowcount() - 1) && (emptylines_bottom == 0)) {
        displine_first++;
        ptr = NULL; /* displine_first ptr needs to be recomputed now */
      } else if ((int)y > fileposcursor) {
        key = 0x151; /* simulate pgdown */
      } else if ((int)y < fileposcursor) {
        key = 0x149; /* simulate pgup */
      }
    } else {
      goto WAITEVENT; /* ignore mouse clicks on invalid screen areas and do not waste time on screen refresh */
    }
  }
  ptui_mouseshow(0); /* hide mouse to avoid "mouse droppings" in DOS (glitches due to direct VRAM access) */

  switch (key) {
    case 0x1b:      /* ESCAPE */
      if (esc_quits) {
        warn("PRESS ESC AGAIN TO QUIT", NULL, colscheme);
        if (ptui_getkey() == 0x1b) return(ACTION_QUIT);
      } else {
        return(ACTION_PREV);
      }
      break;
    case 0x148:      /* UP */
      if (cursory > 1) {
        refreshonlylines[0] = displine_first + cursory - 1;
        cursory--;
        refreshonlylines[1] = displine_first + cursory - 1;
      } else { /* scroll document, if possible */
        if (displine_first > 0) {
          displine_first--;
          ptr = NULL; /* displine_first ptr needs to be recomputed now */
        } else {
          refreshonlylines[0] = 0;
        }
      }
      break;
    case 0x149:      /* PGUP */
      if (displine_first >= (ptui_getrowcount() - 2)) {
        displine_first -= ptui_getrowcount() - 2;
        ptr = NULL; /* displine_first ptr needs to be recomputed now */
        break;
      }
      /* Fall through */
    case 0x147:      /* HOME */
      if (displine_first) {
        displine_first = 0;
        refreshonlylines[0] = 0xffffu;
        ptr = NULL; /* displine_first ptr needs to be recomputed now */
      }
      cursory = 1;
      break;
    case 0x150:      /* DOWN */
      if (cursory < lastcontentline - 1) {
        refreshonlylines[0] = displine_first + cursory - 1;
        cursory++;
        refreshonlylines[1] = displine_first + cursory - 1;
      } else {  /* scroll document down, if possible */
        if (emptylines_bottom == 0) {
          displine_first++;
          ptr = NULL; /* displine_first ptr needs to be recomputed now */
        } else {
          cursory = lastcontentline;
          refreshonlylines[0] = 0;
        }
      }
      break;
    case 0x151:      /* PGDOWN */
      if (emptylines_bottom == 0) {
        displine_first += (ptui_getrowcount() - 2);
        ptr = NULL; /* displine_first ptr needs to be recomputed now */
        break;
      }
      /* Fall through */
    case 0x14F:      /* END */
      if (displine_first < totlinescount - (ptui_getrowcount() - 2)) {
        displine_first = totlinescount - (ptui_getrowcount() - 2);
        refreshonlylines[0] = 0xffffu;
        ptr = NULL; /* displine_first ptr needs to be recomputed now */
      }
      cursory = lastcontentline;
      break;
    case 0x14D:      /* RIGHT */
      if (cursorx < 78) {
        cursorx++;
        refreshonlylines[0] = displine_first + cursory - 1;
      }
      break;
    case 0x14B:      /* LEFT */
      if (cursorx > 1) {
        cursorx--;
        refreshonlylines[0] = displine_first + cursory - 1;
      }
      break;
    case 0x09:       /* TAB */
    {
      unsigned short line;
      line = find_next_link(body, ptr, displine_first, &cursorx, cursory);
      if (line != 0xffffu) {
        if (line < displine_first) {
          displine_first = line;
          cursory = 1;
          /* if not line 0, then offset display one line down (nicer) */
          if (displine_first > 0) {
            displine_first--;
            cursory++;
          }
          ptr = NULL; /* displine_first ptr needs to be recomputed now */
          /* conditions below means "scroll the page down if next link is not on the same line as the current cursor position and it is either at the last displayed line or below" */
        } else if ((line != displine_first + cursory - 1) && (line >= displine_first + ptui_getrowcount() - 2)) {
          displine_first = line - (ptui_getrowcount() - 3);
          cursory = ptui_getrowcount() - 2;
          ptr = NULL; /* displine_first ptr needs to be recomputed now */
        } else {
          refreshonlylines[0] = displine_first + cursory - 1;
          cursory = (line - displine_first) + 1;
          refreshonlylines[1] = displine_first + cursory - 1;
        }
      }
      break;
    }
    case 0x0d:       /* ENTER */
      if (linkover != NULL) {
        char fname[13];
        int i;
        /* copy linkover into fname, up to 12 chars */
        for (i = 0; i < 12; i++) {
          if (linkover[i] == 0) break;
          if (linkover[i] == ':') break;
          if (linkover[i] == '\n') break;
          if (linkover[i] == '\r') break;
          fname[i] = linkover[i];
        }
        fname[i] = 0;
        /* resolve fname into newtarget */
        *newtarget = ama_findslot(fname, ambfcount, fd, &i);
        if (i == 0) return(ACTION_LOADPAGE);
        /* findslot() failed to locate the file */
        warn("NOT FOUND:", fname, colscheme);
        ptui_getkey();
      } else {
        refreshonlylines[0] = 0;
      }
      break;
    default:
#ifdef PRINT_KEYCODES
      ptui_locate(1, 1);
      printf("key = 0x%02x\n", key);
#endif
      break;
  }
  goto AGAIN;
}


/* returns length of file on success and fills fname with file name (unless fname is NULL), returns 0 otherwise */
static unsigned short ama_load(char far *mem, unsigned short slot, char *fname, FILEHANDLE fd) {
  unsigned long ama_offset;
  unsigned short ama_len;
  unsigned char fentry[20]; /* must be unsigned! */

  /* read file entry */
  ama_offset = slot; /* doing a multi-step calculation to be sure not to overflow a short */
  ama_offset *= 20;
  ama_offset += 6; /* AMB header is 4 + AMB filescount is 2 */
  FSEEK(fd, ama_offset);
  if (FREAD(fentry, 20, fd) != 20) goto ERR_CORRUPT;

  /* get props */
  ama_offset = fentry[15];
  ama_offset <<= 8;
  ama_offset |= fentry[14];
  ama_offset <<= 8;
  ama_offset |= fentry[13];
  ama_offset <<= 8;
  ama_offset |= fentry[12];
  ama_len = fentry[17];
  ama_len <<= 8;
  ama_len |= fentry[16];
  /* copy filename */
  if (fname != NULL) {
    memcpy(fname, fentry, 12);
    fname[12] = 0;
  }

  /* reposition to start of ama file inside archive and read it */
  FSEEK(fd, ama_offset);
  if (FREAD(mem, ama_len, fd) != ama_len) goto ERR_CORRUPT;
  mem[ama_len] = 0; /* terminate with a NULL byte */
  return(ama_len);

  ERR_CORRUPT:
  mystrcpy(mem, "%!ERROR: FILE IS CORRUPTED");
  return(0);
}


static void draw_titlebar_curfile(const char *amafname, const unsigned char *col) {
  int i = 66; /* start drawing at offset 66 (left of this is done by draw_titlebar_title) */
  int fnamelen;
  int off;
  /* count how many bytes there is in amafname until end of string or '.' */
  for (fnamelen = 0; (amafname[fnamelen] != '.') && (amafname[fnamelen] != 0); fnamelen++);
  /* add some padding so fname is right-aligned */
  for (; i < (77 - fnamelen); i++) ptui_putchar(' ', col[AMB_TITLEBAR], i, 0);
  /* ama fname - display it now (right-aligned to screen) */
  ptui_putchar('[', col[AMB_TITLEBAR], i++, 0);
  for (off = i; (i - off) < fnamelen; i++) {
    ptui_putchar(amafname[i - off], col[AMB_TITLEBAR], i, 0);
  }
  ptui_putchar(']', col[AMB_TITLEBAR], i++, 0);
}


static void draw_titlebar_title(const char far *ambtitle, const unsigned char *col) {
  int i = 0, off;
  ptui_putchar(' ', col[AMB_TITLEBAR], i++, 0);
  for (off = i; ambtitle[i - off] != 0; i++) {
    if (ambtitle[i - off] == '\r') break;
    if (ambtitle[i - off] == '\n') break;
    if (i - off > 64) break;
#ifdef UNICODEOUTPUT
    ptui_putchar(UNICODEMAP[(unsigned char)(ambtitle[i - off])], col[AMB_TITLEBAR], i, 0);
#else
    ptui_putchar(ambtitle[i - off], col[AMB_TITLEBAR], i, 0);
#endif
  }
  /* add padding until column 79 */
  for (; i < 80; i++) ptui_putchar(' ', col[AMB_TITLEBAR], i, 0);
}


static void draw_leftborder(const unsigned char *colscheme) {
  int i;
  for (i = ptui_getrowcount() - 1; i > 0; i--) {
    ptui_putchar(VLINE, colscheme[AMB_LBORDER], 0, i);
  }
}


/* add a new entry to history */
static void history_push(unsigned short *history, int *history_cur, unsigned short newentry) {
  int i, cur;
  cur = *history_cur;
  /* make room if history buff full */
  if (cur == HISTORYSIZE) {
    for (i = 1; i < HISTORYSIZE; i++) history[i - 1] = history[i];
    cur--;
  }
  /* append new entry to list and inc history_cur (and return it) */
  history[cur] = newentry;
  *history_cur = cur + 1;
}


static unsigned short history_pop(unsigned short *history, int *history_cur, unsigned short defaultslot) {
  int cur;
  cur = *history_cur;
  if (cur == 0) return(defaultslot);
  cur--;
  *history_cur = cur;
  return(history[cur]);
}


enum COLSCHEMES {
  COLSCH_MONO,
  COLSCH_COLOR
};


static void loadcolscheme(unsigned char *colscheme, enum COLSCHEMES id) {
  switch (id) {
    case COLSCH_COLOR:
      colscheme[AMB_TITLEBAR] = 0x70;
      colscheme[AMB_LBORDER] = 0x17;
      colscheme[AMB_RBORDER] = 0x70;
      colscheme[AMB_RBORDERC] = 0x00;
      colscheme[AMB_WARN] = 0x4e;
      colscheme[AMA_LINK_CURSOROVER] = 0x2f;
      colscheme[AMA_NORMAL] = 0x17;
      colscheme[AMA_BORING] = 0x18;
      colscheme[AMA_LINK] = 0x1e;
      colscheme[AMA_HEAD] = 0x1F;
      colscheme[AMA_NOTICE] = 0x60;
      break;
    case COLSCH_MONO:
      colscheme[AMB_TITLEBAR] = 0x70;
      colscheme[AMB_LBORDER] = 0x07;
      colscheme[AMB_RBORDER] = 0x07;
      colscheme[AMB_RBORDERC] = 0x70;
      colscheme[AMB_WARN] = 0x70;
      colscheme[AMA_LINK_CURSOROVER] = 0xf0;
      colscheme[AMA_NORMAL] = 0x07;
      colscheme[AMA_BORING] = 0x07;
      colscheme[AMA_LINK] = 0x0f;
      colscheme[AMA_HEAD] = 0x01; /* MDA-compatible cards render this as "underlined" */
      colscheme[AMA_NOTICE] = 0x70;
      break;
  }
}


int main(int argc, char **argv) {
  FILEHANDLE fd = FILEHANDLE_NONE;
  char *errmsg = NULL;
  unsigned short filescount;
  unsigned char colscheme[AMA_HIDDEN];
  enum ACTION action;
  int ptui_inited = 0;
  unsigned short history[HISTORYSIZE];
  int history_cur = 0;
  unsigned short curslot;
  unsigned short index_slot;
  int errflag;
#ifdef REALDOS
  static char staticbuff[50 * 1024u]; /* can't have full 64K in a COM file, sorry */
  /* DOS note: When DOS loads a COM file, it tries to find a 64K block of available
   * memory, copies there the 256-bytes PSP followed by the COM file, sets CS=DS=SS to
   * the segment where PSP starts, sets SP to 64K-2 and sets IP to 0x100.
   * This means that 64K of memory is wasted anyway - so better filling it up
   * with something useful (like the above buffer). The memory map produced by TCC
   * might suggest something different, but examining the values held by _DS, _SS and
   * _SP registers at runtime leaves no doubt.
   * One could think that using an EXE instead would be more efficient, but my tests
   * show that Turbo C 2.01 does not behave much differently when creating an EXE
   * binary (checked under small memory model): only difference is that CS gets its
   * own segment, but DS=SS still and SP is set at the end of the segment (65xxx). */
  char far *ama = staticbuff;
#else
  static char ama[65536u];
#endif

  if ((argc < 2) || (argc > 3) || (argv[1][0] == '/')) {
    errmsg = "usage: amb file.amb [chaptername]";
    goto EARLY_QUIT;
  }

  fd = FOPEN(argv[1]);
  if (fd == FILEHANDLE_NONE) {
    errmsg = "ERROR: failed to open file";
    goto EARLY_QUIT;
  }

  if ((FREAD(ama, 4, fd) != 4) || (ama[0] != 'A') || (ama[1] != 'M') || (ama[2] != 'B') || (ama[3] != '1') || (FREAD(ama, 2, fd) != 2)) {
    errmsg = "ERROR: invalid file format";
    goto EARLY_QUIT;
  }

  filescount = ama[1];
  filescount <<= 8;
  filescount |= ama[0];

  if (filescount == 0) {
    errmsg = "ERROR: AMB archive is empty";
    goto EARLY_QUIT;
  }

  /* look how big the biggest file is in the AMB archive, so I know whether
   * or not I need to allocate a "big" memory buffer (16-bit DOS only)
   * NOTE: I never free the allocated block, DOS does that at program's end */
#ifdef REALDOS
  {
    unsigned short i;
    unsigned short biggest = 0;
    unsigned short sz;
    for (i = filescount; i != 0; i--) {
      if (FREAD(ama, 20, fd) != 20) {
      }
      sz = (unsigned char)(ama[17]);
      sz <<= 8;
      sz |= (unsigned char)(ama[16]);
      if (sz > biggest) biggest = sz;
    }
    /* do I need a buffer bigger than my static buffer? */
    if (biggest >= sizeof(staticbuff)) {
      ama = ALLOC64K();
      if (ama == NULL) {
        errmsg = "ERROR: out of memory";
        goto EARLY_QUIT;
      }
    }
  }
#endif

  /* look for specific chapter if user asked for it */
  if (argc == 3) {
    char userchap[16];
    int i;
    for (i = 0; i < 13; i++) {
      userchap[i] = argv[2][i];
      if ((userchap[i] == 0) || (userchap[i] == '.')) break;
    }
    /* append the .ama extension */
    userchap[i++] = '.';
    userchap[i++] = 'a';
    userchap[i++] = 'm';
    userchap[i++] = 'a';
    userchap[i++] = 0;
    /* look for the exact thing that user asked for, if not found try again with appending .ama */
    index_slot = ama_findslot(argv[2], filescount, fd, &errflag);
    if (errflag != 0) {
      index_slot = ama_findslot(userchap, filescount, fd, &errflag);
      if (errflag != 0) {
        errmsg = "ERROR: requested chapter not found";
        goto EARLY_QUIT;
      }
    }
  } else {  /* locate the default index slot */
    index_slot = ama_findslot("index.ama", filescount, fd, &errflag);
    if (errflag != 0) {
      errmsg = "ERROR: this AMB file does not contain an index.ama";
      goto EARLY_QUIT;
    }
  }

  /* on unicode-aware platforms set up the CP->unicode map table */
#ifdef UNICODEOUTPUT
{
  /* first 128 entries are normal ascii */
  unsigned short i;
  for (i = 0; i < 128; i++) UNICODEMAP[i] = i;
  for (i = 128; i < 256; i++) UNICODEMAP[i] = 0xfffdu; /* preload high-ascii with the unicode 'replacement' character */
  /* does this AMB feature a unicode.map? */
  i = ama_findslot("unicode.map", filescount, fd, &errflag);
  if (errflag == 0) {
    ama_load(ama, i, NULL, fd);
    for (i = 0; i < 128; i++) UNICODEMAP[128 + i] = ((unsigned char)ama[i * 2]) | (((unsigned char)ama[i * 2 + 1]) << 8) ;
  }
}
#endif

  /* file is open, memory is allocated, index found: all seems fine */
  if (ptui_init(PTUI_ENABLE_MOUSE) != 0) {
    PUTS("ERROR: UI initialization failed");
    goto EARLY_QUIT;
  }
  ptui_inited = 1; /* remember that ptui is inited to close it later */
  ptui_mouseshow(0); /* hide mouse by default -- will be shown by display_ama() */

  /* init color scheme */
  if (ptui_hascolor()) {
    loadcolscheme(colscheme, COLSCH_COLOR);
  } else {
    loadcolscheme(colscheme, COLSCH_MONO);
  }

  action = ACTION_LOADPAGE;
  curslot = index_slot;

  /* draw the title bar */
  {
    unsigned short titslot;
    titslot = ama_findslot("title", filescount, fd, &errflag);
    if (errflag == 0) {
      ama_load(ama, titslot, NULL, fd);
      draw_titlebar_title(ama, colscheme);
    } else { /* if no title found, use the amb filename */
      draw_titlebar_title(argv[1], colscheme);
    }
  }

  /* draw the left border */
  draw_leftborder(colscheme);

  /* main loop */
  for (;;) {
    unsigned short newslot;
    switch (action) {
      case ACTION_PREV:
      case ACTION_LOADPAGE:
        {
          char fname[13];
          ama_load(ama, curslot, fname, fd);
          draw_titlebar_curfile(fname, colscheme);
        }
        break;
      case ACTION_QUIT:
        goto EARLY_QUIT;
      case ACTION_NONE:
        /* no-op */
        break;
    }

    action = display_ama(ama, colscheme, &newslot, fd, filescount, ((history_cur == 0) && (curslot == index_slot))?1:0);

    if (action == ACTION_PREV) {
      curslot = history_pop(history, &history_cur, index_slot);
    } else if ((action == ACTION_LOADPAGE) && (curslot != newslot)) { /* push to history only if page is different */
      history_push(history, &history_cur, curslot);
      curslot = newslot;
    }
  }

  /* exit */

  EARLY_QUIT:
  if (ptui_inited) {
    ptui_cls();
    ptui_close();
  }
  if (fd != FILEHANDLE_NONE) FCLOSE(fd);
  if (errmsg != NULL) {
    PUTS(errmsg);
    return(1);
  }

  PUTS("AMB ver " PVER);
  PUTS("Have a nice day!");

  return(0);
}
