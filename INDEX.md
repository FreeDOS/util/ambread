# AMB Reader

AMBREAD is a reader of AMB (Ancient Machine Book) files. AMB is an extremely lightweight file format meant to store any kind of hypertext documentation that may be comfortably viewed even on the most ancient PCs: technical manuals, books, etc.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## AMBREAD.LSM

<table>
<tr><td>title</td><td>AMB Reader</td></tr>
<tr><td>version</td><td>20250110</td></tr>
<tr><td>entered&nbsp;date</td><td>2025-01-10</td></tr>
<tr><td>description</td><td>Reader for Ancient Machine Book files</td></tr>
<tr><td>summary</td><td>AMBREAD is a reader of AMB (Ancient Machine Book) files. AMB is an extremely lightweight file format meant to store any kind of hypertext documentation that may be comfortably viewed even on the most ancient PCs: technical manuals, books, etc.</td></tr>
<tr><td>keywords</td><td>text, doc, reader</td></tr>
<tr><td>author</td><td>Mateusz Viste</td></tr>
<tr><td>maintained&nbsp;by</td><td>Mateusz Viste</td></tr>
<tr><td>primary&nbsp;site</td><td>https://ambook.sourceforge.net/</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://sourceforge.net/projects/ambook/</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[MIT](LICENSE)</td></tr>
</table>
